/* global require, PluginHost, App, Plugins */

require(['dojo/_base/kernel', 'dojo/ready'], function  (dojo, ready) {
	ready(function() {
		Plugins.Grid_Masonry = {
			rows: [],
			timeouts: [],
			resize_observer: new ResizeObserver((entries) => {
				console.log('grid_masonry', 'resize_observer', entries, entries.length);

				entries.forEach((entry) => Plugins.Grid_Masonry.fit_next_of(entry.target));
			}),
			mutation_observer: new MutationObserver((mutations) => {
				mutations.forEach((mutation) => {
					if (mutation.type == "attributes" && mutation.attributeName == "class") {
						console.log('grid_masonry', 'mutation_observer', mutation);
						Plugins.Grid_Masonry.fit_row(mutation.target)
					}
				})
			}),
			intersection_observer: new IntersectionObserver((entries) => {
				console.log('grid_masonry', 'intersection_observer', entries, entries.length);
				entries.forEach((entry) => {
					Plugins.Grid_Masonry.fit_row(entry.target)
				});
				}, {root: document.querySelector("#headlines-frame")}
			),
			get_columns: function() {
				return window.getComputedStyle(App.byId('headlines-frame')).gridTemplateColumns.split(' ').length;
			},
			fit_next_of: function(row) {
				const num_columns = this.get_columns();
				const row_idx = this.rows.indexOf(row);
				const next = this.rows[row_idx + num_columns];

				if (next)
					this.fit_row(next);
			},
			fit_row: function(row) {

				// still packed
				if (row.hasAttribute('data-content'))
					return;

				if (window.getComputedStyle(row).gridColumn != "auto / auto") {
					row.style.marginTop = '0px';
					return;
				}

				const num_columns = this.get_columns();
				const row_idx = this.rows.indexOf(row);
				const prev = this.rows[row_idx - num_columns];

				if (prev) {
					if (window.getComputedStyle(prev).gridColumn != "auto / auto") {
						row.style.marginTop = '0px';
					} else {
						const delta_height = prev.offsetHeight - prev.querySelector('.content').offsetHeight - prev.querySelector('.header').offsetHeight;
						console.log('grid_masonry', prev.id, row.id, delta_height);
						row.style.marginTop = `${-delta_height}px`;
					}
				}
			},
			init: function() {
				if (!App.isCombinedMode() || !App.getInitParam("cdm_expanded") || !App.getInitParam("cdm_enable_grid"))
					return;

				this.rows = [...App.findAll("div[id*=RROW]")];

				this.rows.forEach((row) => {
					this.resize_observer.observe(row);
					this.intersection_observer.observe(row);

					// expand-shrink button
					this.mutation_observer.observe(row, {attributes: true});
				})
			}
		}

		PluginHost.register(PluginHost.HOOK_HEADLINES_RENDERED, function() {
			Plugins.Grid_Masonry.init();
			return true;
		});
	});
});
